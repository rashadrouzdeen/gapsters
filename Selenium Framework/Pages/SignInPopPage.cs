﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 @name: rashad rouzdeen
 @author: mrmrashad88@gmail.com
 */

namespace Selenium_Framework.Pages
{
    class SignInPopPage
    {

        public SignInPopPage()
        {
            PageFactory.InitElements(PropertiesCollection.Driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//div/a[text()='or use email »']")]
        public IWebElement linkUseEmail { get; set; }

        [FindsBy(How = How.XPath, Using = "//div/a[text()='Connect with Facebook']")]
        public IWebElement linkConnectWithFB { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='input-group']/descendant::input[@name='email']")]
        public IWebElement txtEmail { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='input-group']/descendant::button[@class='btn btn-xl btn-success']")]
        public IWebElement BtnEmailSubmit { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='input-group']/descendant::input[@name='password']")]
        public IWebElement txtPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='email_container']/descendant::input[@id='email']")]
        public IWebElement txtFBEmail { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='clearfix _5466 _44mg']/descendant::input[@id='pass']")]
        public IWebElement txtFBPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='_xkt']/descendant::button[@id='loginbutton']")]
        public IWebElement BtnFBSubmit { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='_2mgi _4k6n']/descedant::button[@name='__CONFIRM__']")]
        public IWebElement BtnFBContinueAsUser { get; set; }

    }
}
