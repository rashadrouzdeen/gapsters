﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 @name: rashad rouzdeen
 @author: mrmrashad88@gmail.com
 */

namespace Selenium_Framework.Pages
{
    class TestClassPage
    {

        public TestClassPage()
        {
            PageFactory.InitElements(PropertiesCollection.Driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//span[@class='icon-hamburger-animated']")]
        public IWebElement btnBurgerSideMenu { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='sidemenu-items']/descendant::a[text()='Sign in']")]
        public IWebElement BtnSignIn { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='sidemenu-alert']/descendant::a[text()='Delete this book']")]
        public IWebElement btnDeleteBook { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='sidemenu-alert-box']/descendant::a[@class='btn btn-xs btn-danger cta-book-delete']")]
        public IWebElement btnDeleteBookYes { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='sidemenu-items']/descendant::a[text()='Sign out']")]
        public IWebElement BtnSignout { get; set; }


    }
}
