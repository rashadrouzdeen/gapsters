﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 @name: rashad rouzdeen
 @author: mrmrashad88@gmail.com
 */

namespace Selenium_Framework.Pages
{
    class CreateBookPage
    {

        public CreateBookPage()
        {
            PageFactory.InitElements(PropertiesCollection.Driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//section[@class='base']/descendant::input[@name='title']")]
        public IWebElement txtBookTitle { get; set; }

        [FindsBy(How = How.XPath, Using = "//section[@class='base']/descendant::button[@id='create-button']")]
        public IWebElement btnCreateButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='row']/descendant::a[text()='Upload your pictures']")]
        public IWebElement btnUploadPictures { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='fsp-content']/descendant::input[@id='fsp-fileUpload']")]
        public IWebElement btnUploadFiles { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='container']/descendant::a[text()=' Add pictures']")]
        public IWebElement btnUploadPictures_added { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@title='Upload']/descendant::span[text()=' Upload ']")]
        public IWebElement btnUploadButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='bootbox-image-checkbox active']/descendant::div/img[@title='Full page']")]
        public IWebElement imgFullImage { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-footer']/descendant::button[@data-bb-handler='addExp']/b[text()='Continue »']")]
        public IWebElement btnContinue { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='book-closed-message book-closed-message-left hidden-xs hidden-sm']/descendant::button[text()='Open your book']")]
        public IWebElement btnOpenYourBook { get; set; }

    }
}
