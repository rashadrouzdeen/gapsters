﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using NUnit.Framework;
using Selenium_Framework.Functions;
using Selenium_Framework.Pages;
using System;

/*
 @name: rashad rouzdeen
 @author: mrmrashad88@gmail.com
 @testsase: Sign in through gmail and create a book
 */

namespace Selenium_Framework
{
	[TestFixture]
	class LoginEmail : Hooks
	{
		private static ExtentReports extent;
		private ExtentTest childTest1;


		public LoginEmail() : base(BrowserType.Firefox)
		{
		}
		[OneTimeSetUp]
		public void SetupReporting()
		{
			extent = ExtentManager.GetExtent();
			
			//Create Extent Test
			ExtentTest test = extent.CreateTest("Sign In Using Email", "Create book with photoes");
			childTest1 = test.CreateNode("Test signin / signout and createbook");
			
		}
		[SetUp]
		public void Initialize()
		{
			//Navigate to ASPDotNetPage from Chrome
			Driver.Navigate().GoToUrl(Prop_Values.Property_values.URL);
			Driver.Manage().Window.Maximize();
		}


		[Test]
		public void EmailLogin()
		{
			try
			{
				/*Functions and Page class Initialization*/
				TestFunc testFunc = new TestFunc();
                TestClassPage testclasspage = new TestClassPage();
                SignInPopPage signInPopPage = new SignInPopPage();
				CreateBookPage createBookPage = new CreateBookPage();


				childTest1.Info("Entering to pastbook site");
				childTest1.Info("Click burger icon");
				childTest1.Info("Click Signin");
                testFunc.signin();
				System.Threading.Thread.Sleep(2000);

				//switch to new window
				childTest1.Info("Switch to new window");
                Driver.SwitchTo().Window(Driver.WindowHandles[1]);
                childTest1.Info("Enter Email id");
				childTest1.Info("Enter Password");
				testFunc.TestEmailLogin();
                System.Threading.Thread.Sleep(5000);

				//switch back to old window
				childTest1.Info("Switch back to old window");
				Driver.SwitchTo().Window(Driver.WindowHandles[0]);
                System.Threading.Thread.Sleep(2000);
				childTest1.Info("Enter Book Title");
				testFunc.BookTitle();
                System.Threading.Thread.Sleep(2000);
				childTest1.Info("Upload Image");
				testFunc.uploadImage();
                System.Threading.Thread.Sleep(2000);
				childTest1.Info("Select Page Cover");
				testFunc.selectPage();
				System.Threading.Thread.Sleep(2000);

				try
				{
					Assert.IsTrue(createBookPage.btnOpenYourBook.Displayed);
					childTest1.Info("User Should be able to create a book with Fullpage cover");
					childTest1.Pass("Assertion passed");

					string screenShotPath = ScreenshotReport.Capture(Driver, "EmailLoginPassed");
					childTest1.Log(childTest1.Status, "Snapshot below: " + childTest1.AddScreenCaptureFromPath(screenShotPath));
				}
				catch (AssertionException)
				{
					childTest1.Info("User cannot create a book with Fullpage cover");
					childTest1.Fail("Assertion failed");
					string screenShotPath = ScreenshotReport.Capture(Driver, "EmailLoginFailed");
					childTest1.Log(childTest1.Status, "Snapshot below: " + childTest1.AddScreenCaptureFromPath(screenShotPath));
					throw;
				}

				childTest1.Info("Delete Book");
				testFunc.deleteBook();
                System.Threading.Thread.Sleep(2000);
				childTest1.Info("Signout");
				testFunc.signout();
				System.Threading.Thread.Sleep(2000);
				childTest1.Info("Signin Again");
				testFunc.signin();
                System.Threading.Thread.Sleep(2000);


				//switch to new window
				childTest1.Info("Switch to new window");
				Driver.SwitchTo().Window(Driver.WindowHandles[1]);
				childTest1.Info("Enter Email id");
				childTest1.Info("Enter Password");
				testFunc.TestEmailLogin();
				System.Threading.Thread.Sleep(5000);
				Driver.SwitchTo().Window(Driver.WindowHandles[0]);
                System.Threading.Thread.Sleep(2000);
				//switch back to old window
				childTest1.Info("Switch back to old window");
				Driver.SwitchTo().Window(Driver.WindowHandles[0]);
				System.Threading.Thread.Sleep(2000);

				extent.Flush();
			}
			catch (Exception e)
			{
				childTest1.Fail("Test Exception Failed");
				childTest1.Fail(e.Message);
				string screenShotPath = ScreenshotReport.Capture(Driver, "EmailLoginToPassBookFailed");
				childTest1.Log(childTest1.Status, "Snapshot below: " + childTest1.AddScreenCaptureFromPath(screenShotPath));
				extent.Flush();
				throw new Exception(e.Message);
			}
		}
		[TearDown]
		public void CleanUp()
		{
			// Closing Chrome Driver
			extent.Flush();
			Driver.Close();
		}
		
	}


}
