﻿using NUnit.Framework;
using Selenium_Framework.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 @name: rashad rouzdeen
 @author: mrmrashad88@gmail.com
 */

namespace Selenium_Framework.Functions
{
    class TestFunc
    {
        public void signin()
        {
            TestClassPage test = new TestClassPage();

            test.btnBurgerSideMenu.Click();
            System.Threading.Thread.Sleep(5000);
            test.BtnSignIn.Click();
            System.Threading.Thread.Sleep(5000);

        }

        public void TestEmailLogin()
        {
            SignInPopPage signInPopPage = new SignInPopPage();

            System.Threading.Thread.Sleep(2000);
            signInPopPage.linkUseEmail.Click();
            System.Threading.Thread.Sleep(2000);
            signInPopPage.txtEmail.EnterText(Prop_Values.Property_values.email);
            System.Threading.Thread.Sleep(2000);
            signInPopPage.BtnEmailSubmit.Click();
            System.Threading.Thread.Sleep(5000);
            signInPopPage.txtPassword.EnterText(Prop_Values.Property_values.password);
            System.Threading.Thread.Sleep(2000);
            signInPopPage.BtnEmailSubmit.Click();
            System.Threading.Thread.Sleep(5000);

        }

        public void TestFacebookLogin()
        {
            SignInPopPage signInPopPage = new SignInPopPage();
            System.Threading.Thread.Sleep(2000);
            signInPopPage.linkConnectWithFB.Click();
            System.Threading.Thread.Sleep(2000);
            signInPopPage.txtFBEmail.EnterText(Prop_Values.Property_values.email);
            System.Threading.Thread.Sleep(2000);
            signInPopPage.txtFBPassword.EnterText(Prop_Values.Property_values.FBPassword);
            System.Threading.Thread.Sleep(2000);
            signInPopPage.BtnFBSubmit.Click();
            System.Threading.Thread.Sleep(2000);
           
        }

        public void TestFacebookLogin_SecondTime()
        {
            SignInPopPage signInPopPage = new SignInPopPage();
            System.Threading.Thread.Sleep(2000);
            signInPopPage.linkConnectWithFB.Click();
        }

            public void BookTitle()
        {
            SignInPopPage signInPopPage = new SignInPopPage();
            CreateBookPage createBookPage = new CreateBookPage();
            

            System.Threading.Thread.Sleep(2000);
            createBookPage.txtBookTitle.EnterText(Prop_Values.Property_values.BookTitle);
            System.Threading.Thread.Sleep(2000);
            createBookPage.btnCreateButton.Click();
            
        }

        public void uploadImage()
        {
            CreateBookPage createBookPage = new CreateBookPage();
            String filePath = "C:\\Users\\User\\Downloads\\Selenium Framework\\train.jpg";

            System.Threading.Thread.Sleep(5000);
            createBookPage.btnUploadPictures.Click();
            System.Threading.Thread.Sleep(2000);
            createBookPage.btnUploadFiles.SendKeys(filePath);
            System.Threading.Thread.Sleep(2000);
            createBookPage.btnUploadButton.Click();
            System.Threading.Thread.Sleep(10000);

        }

        public void selectPage()
        {
            CreateBookPage createBookPage = new CreateBookPage();
            System.Threading.Thread.Sleep(2000);
            createBookPage.imgFullImage.Click();
            System.Threading.Thread.Sleep(2000);
            createBookPage.btnContinue.Click();
            System.Threading.Thread.Sleep(5000);
            
        }
        public void deleteBook() 
        {
            TestClassPage testClassPage = new TestClassPage();

            System.Threading.Thread.Sleep(2000);
            testClassPage.btnBurgerSideMenu.Click();
            System.Threading.Thread.Sleep(2000);
            testClassPage.btnDeleteBook.Click();
            System.Threading.Thread.Sleep(2000);
            testClassPage.btnDeleteBookYes.Click();
            System.Threading.Thread.Sleep(2000);

        }

        public void signout()
        {
            TestClassPage testClassPage = new TestClassPage();

            System.Threading.Thread.Sleep(2000);
            testClassPage.btnBurgerSideMenu.Click();
            System.Threading.Thread.Sleep(2000);
            testClassPage.BtnSignout.Click();
            System.Threading.Thread.Sleep(2000);
        }


    }
}
